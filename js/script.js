/**
 * Класс, объекты которого описывают параметры гамбургера.
 *
 * @constructor
 * @param size        Размер
 * @param stuffing    Начинка
 * @throws {HamburgerException}  При неправильном использовании
 */
function Hamburger(size, stuffing) {

    if (arguments.length < 1) {
        throw new HamburgerException('no size given')
    }
    if (Hamburger.allowedSize.indexOf(size) < 0) {
        throw new HamburgerException(`invalid size '${size.name}'`)
    }
    if (Hamburger.allowedStuffing.indexOf(stuffing) < 0) {
        throw new HamburgerException(`invalid stuffing '${stuffing.name}'`)
    }

    this.size = size;
    this.stuffing = stuffing;
}

/* Размеры, виды начинок и добавок */
Hamburger.SIZE_SMALL = {
    name : 'SIZE_SMALL',
    price : 50,
    calories : 20
};
Hamburger.SIZE_LARGE = {
    name : 'SIZE_LARGE',
    price : 100,
    calories : 40
};
Hamburger.STUFFING_CHEESE = {
    name :'STUFFING_CHEESE',
    price : 10,
    calories : 20
};
Hamburger.STUFFING_SALAD = {
    name : 'STUFFING_SALAD',
    price : 20,
    calories : 5
};
Hamburger.STUFFING_POTATO = {
    name : 'STUFFING_POTATO',
    price: 15,
    calories : 10
};
Hamburger.TOPPING_MAYO = {
    name : 'TOPPING_MAYO',
    price : 20,
    calories :5
};
Hamburger.TOPPING_SPICE = {
    name :'TOPPING_SPICE',
    price : 15,
    calories : 0
};

Hamburger.allowedSize = [Hamburger.SIZE_SMALL, Hamburger.SIZE_LARGE];
Hamburger.allowedStuffing = [Hamburger.STUFFING_CHEESE, Hamburger.STUFFING_POTATO, Hamburger.STUFFING_SALAD];
Hamburger.allowedToppings = [Hamburger.TOPPING_MAYO, Hamburger.TOPPING_SPICE];

/**
 * Добавить добавку к гамбургеру. Можно добавить несколько
 * добавок, при условии, что они разные.
 *
 * @param topping     Тип добавки
 * @throws {HamburgerException}  При неправильном использовании
 */
Hamburger.prototype.addTopping = function (topping) {
    if (arguments.length !== 1) {
        throw new HamburgerException('add a topping')
    }
    if (Hamburger.allowedToppings.indexOf(topping) < 0) {
        throw new HamburgerException(`invalid topping '${topping.name}'`)
    }
    if (!(this.hasOwnProperty('toppings'))) {
        this.toppings = [];
    } else if (this.toppings.indexOf(topping) >= 0) {
        throw new HamburgerException(`duplicate topping '${topping.name}'`)
    }
    this.toppings.push(topping)
};

/**
 * Убрать добавку, при условии, что она ранее была
 * добавлена.
 *
 * @param topping   Тип добавки
 * @throws {HamburgerException}  При неправильном использовании
 */
Hamburger.prototype.removeTopping = function (topping) {
    if (arguments.length !== 1) {
        throw new HamburgerException('add a topping to remove')
    }
    if (Hamburger.allowedToppings.indexOf(topping) < 0)
    {
        throw new HamburgerException(`invalid topping to remove '${topping.name}'`);
    }
    if (this.toppings.indexOf(topping) < 0)
    {
        throw new HamburgerException(`hamburger doesn't have this topping '${topping.name}'`);
    }

    let index = this.toppings.indexOf(topping);
    this.toppings.splice(index,1);
};

/**
 * Получить список добавок.
 *
 * @return {Array} Массив добавленных добавок, содержит константы
 *                 Hamburger.TOPPING_*
 */
Hamburger.prototype.getToppings = function () {
    return this.toppings
};

/**
 * Узнать размер гамбургера
 */
Hamburger.prototype.getSize = function () {
    return this.size
};

/**
 * Узнать начинку гамбургера
 */
Hamburger.prototype.getStuffing = function () {
    return this.stuffing
};

/**
 * Узнать цену гамбургера
 * @return {Number} Цена в тугриках
 */
Hamburger.prototype.calculatePrice = function () {
    let size = this.size;
    let price = size.price;

    let stuffing = this.getStuffing();
        price += stuffing.price;

    let toppings = this.toppings;
    toppings.forEach(function (item) {
        price += item.price
    });

    return price
};

/**
 * Узнать калорийность
 * @return {Number} Калорийность в калориях
 */
Hamburger.prototype.calculateCalories = function () {
    let size = this.size;
    let calories = size.calories;

    let stuffing = this.getStuffing();
    calories += stuffing.calories;

    let toppings = this.toppings;
    toppings.forEach(function (item) {
        calories += item.calories
    });

    return calories
};

/**
 * Представляет информацию об ошибке в ходе работы с гамбургером.
 * Подробности хранятся в свойстве message.
 * @constructor
 */
function HamburgerException (message) {
    this.message = message;
}


// маленький гамбургер с начинкой из сыра
let hamburger = new Hamburger(Hamburger.SIZE_SMALL, Hamburger.STUFFING_CHEESE);
// добавка из майонеза
hamburger.addTopping(Hamburger.TOPPING_MAYO);
// спросим сколько там калорий
console.log("Calories: %f", hamburger.calculateCalories());
// сколько стоит
console.log("Price: %f", hamburger.calculatePrice());
// я тут передумал и решил добавить еще приправу
hamburger.addTopping(Hamburger.TOPPING_SPICE);
// А сколько теперь стоит?
console.log("Price with sauce: %f", hamburger.calculatePrice());
// Проверить, большой ли гамбургер?
console.log("Is hamburger large: %s", hamburger.getSize() === Hamburger.SIZE_LARGE); // -> false
// Убрать добавку
hamburger.removeTopping(Hamburger.TOPPING_SPICE);
console.log("Have %d toppings", hamburger.getToppings().length); // 1


// не передали обязательные параметры
let h2 = new Hamburger(); // => HamburgerException: no size given

// передаем некорректные значения, добавку вместо размера
let h3 = new Hamburger(Hamburger.TOPPING_SPICE, Hamburger.TOPPING_SPICE);
// => HamburgerException: invalid size 'TOPPING_SAUCE'

// добавляем много добавок
let h4 = new Hamburger(Hamburger.SIZE_SMALL, Hamburger.STUFFING_CHEESE);
hamburger.addTopping(Hamburger.TOPPING_MAYO);
hamburger.addTopping(Hamburger.TOPPING_MAYO);
// HamburgerException: duplicate topping 'TOPPING_MAYO'
